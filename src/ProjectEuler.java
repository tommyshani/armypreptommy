public class ProjectEuler {
	
	int sum1 = 0;
	int sum2 = 0;

	
	public void findSumOfSquare() {
		for (int i = 1; i <= 100; i++) {
			sum1 += i*i;
		}
		System.out.print("Sum of squares is = " + sum1);
	}
	
	public void findSquareOfSum() {
		for (int i = 0; i <= 100; i++) {
			sum2 += i;
		}
		int squared = sum2 * sum2;
		System.out.print("Square of sum is = " + squared);
	}
	
	public void findDiff (){ 
		int diff = (sum2 * sum2) - sum1;
		System.out.print("The difference is = " + diff);
	}
	
	public static void main(String[] args){
		ProjectEuler Maya = new ProjectEuler();
		Maya.findSumOfSquare();
		System.out.println();
		Maya.findSquareOfSum();
		System.out.println();
		Maya.findDiff();
	}
}
